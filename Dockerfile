#FROM scratch desde cero (0)
#Imagen inicial a partir de la cual creamos nuestra imagen
FROM node
#Se define el directorio del contenedor
WORKDIR /colbbva_oipp
#Adiciona el contenido del proyecto en el directorio del contenedor
ADD . /colbbva_oipp
#Puerto por el que escucha el contenedor
EXPOSE 4000
#Comandos para lanzar nuestra API REST 'colbbva'
CMD ["npm", "start"]
#En produccion debe ir como node server.js
#CMD ["node", "server.js"]
