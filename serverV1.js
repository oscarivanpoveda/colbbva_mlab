//Declaraciones y parametros
//Libreria Express
var express = require('express');
var app = express();
//Libreria Request - json
var requestJSON = require('request-json');
//Variable para archivo externo
//var usersFile = require('./file.json');
//var usersFile = require('./users.json');
//Deaclara la URL base para todo el programa
var URLbase = '/colbbva/v3/';
//Apunta o importa la libreria body parser
//Utiliza parasar un json
//app.use(bodyParser.xml());
var bodyParser = require('body-parser');
app.use(bodyParser.json());
//Variables para definir la URL de MLab
var baseMLabURL = 'https://api.mlab.com/api/1/databases/colbbva_oipp/collections/';
var apiKeyMLab = 'apiKey=80aJU7mCzZprpI8uqblWDwEl86jCEaNs';
//Puerto por el que esta escuchando la aplicacion
var port = process.env.PORT || 4000;
app.listen(port);
console.log('App port listen '+port+'!!!');

//GET users a traves de mLab con parametros
app.get(URLbase + 'users/:id',
  function(request, response){
      var idParam = request.params.id;
      var httpClient = requestJSON.createClient(baseMLabURL);
      var queryString = 'q={"id":' + idParam + '}&';
      httpClient.get('user?'+ queryString + apiKeyMLab,
          function(e, respuestaMLab, body){
              var answer = body[0];
              response.send(answer);
          });
});

//POST con mLab
app.post(URLbase + 'users', function(req, res) {
  var newUser = {
    "id" : req.body.id,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : req.body.password
  };
  var clientMlab = requestJSON.createClient(baseMLabURL + "/user?" + apiKeyMLab);
  clientMlab.post('', req.body, function(err, resM, body) {
    res.send(body);
  });
});

//PUT con MLab
app.put(URLbase + '/users/:id', function(req, res) {
 clienteMlab = requestJson.createClient(baseMLabURL + "/user");
 var cambio = '{"$set":' + JSON.stringify(req.body) + '}'
 clienteMlab.put('?q={"idusuario": ' + req.params.id + '}&' + apiKey, JSON.parse(cambio), function(err, resM, body) {
   res.send(body);
 });
});

//GET movement a traves de mLab
app.get(URLbase + 'movements',
  function(request, response){
      console.log('GET /Movements');
      var httpClient = requestJSON.createClient(baseMLabURL);
      var queryString = 'f={"_id":0}&';
      httpClient.get('movement?'+ queryString + apiKeyMLab,
          function(e, respuestaMLab, body){
              console.log('Error ' + e);
              console.log('Body ' + body);
              console.log('Respuesta MLab ' + respuestaMLab);
              var respuesta = body;
              respuesta = !e ? body : {"msg":"Error al hacer la consulta"};
              response.send(respuesta);
          });
});

//GET movement a traves de mLab con parametros
app.get(URLbase + 'movements/:id',
  function(request, response){
      var idParam = request.params.id;
      var httpClient = requestJSON.createClient(baseMLabURL);
      var queryString = 'q={"id_mov":' + idParam + '}&';
      httpClient.get('movement?'+ queryString + apiKeyMLab,
          function(e, respuestaMLab, body){
              var answer = body[0];
              response.send(answer);
          });
});

//GET account a traves de mLab
app.get(URLbase + 'accounts',
  function(request, response){
      console.log('GET /Accounts');
      var httpClient = requestJSON.createClient(baseMLabURL);
      var queryString = 'f={"_id":0}&';
      httpClient.get('account?'+ queryString + apiKeyMLab,
          function(e, respuestaMLab, body){
              console.log('Error ' + e);
              console.log('Body ' + body);
              console.log('Respuesta MLab ' + respuestaMLab);
              var respuesta = body;
              respuesta = !e ? body : {"msg":"Error al hacer la consulta"};
              response.send(respuesta);
          });
});

//GET account a traves de mLab con parametros
app.get(URLbase + 'accounts/:id',
  function(request, response){
      var idParam = request.params.id;
      var httpClient = requestJSON.createClient(baseMLabURL);
      var queryString = 'q={"user_id":' + idParam + '}&';
      httpClient.get('account?'+ queryString + apiKeyMLab,
          function(e, respuestaMLab, body){
              var answer = body[0];
              response.send(answer);
          });
});

//GET users a traves de mLab
app.get(URLbase + 'users',
  function(request, response){
      console.log('GET /Users');
      var httpClient = requestJSON.createClient(baseMLabURL);
      console.log('Cliente HTTP mLab GET Ok');
      var queryString = 'f={"_id":0}&';
      httpClient.get('user?'+ queryString + apiKeyMLab,
          function(e, respuestaMLab, body){
              console.log('Error ' + e);
              console.log('Body ' + body);
              console.log('Respuesta MLab ' + respuestaMLab);
              var respuesta = body;
              respuesta = !e ? body : {"msg":"Error al hacer la consulta"};
              response.send(respuesta);
          });
});

//GET users a traves de mLab con parametros
app.get(URLbase + 'users/:id',
  function(request, response){
      var idParam = request.params.id;
      var httpClient = requestJSON.createClient(baseMLabURL);
      var queryString = 'q={"id":' + idParam + '}&';
      httpClient.get('user?'+ queryString + apiKeyMLab,
          function(e, respuestaMLab, body){
              var answer = body[0];
              response.send(answer);
          });
});

//GET users a traves de mLab con parametros
app.get(URLbase + 'users/:id/accounts',
  function(request, response){
      var idParam = request.params.id;
      var httpClient = requestJSON.createClient(baseMLabURL);
      var queryString = 'q={"user_id":' + idParam + '}&';
      httpClient.get('account?'+ queryString + apiKeyMLab,
          function(e, respuestaMLab, body){
              var answer = body;
              response.send(answer);
          });
});
