//Libreria Express
var express = require('express');
var app = express();
//Libreria Request - json
var requestJSON = require('request-json');
//Variable para archivo externo
var usersFile = require('./file.json');
//Deaclara la URL base para todo el programa
var URLbase = '/colbbva/v3/';
//Apunta o importa la libreria body parser
var bodyParser = require('body-parser');
app.use(bodyParser.json());
//Variables para definir la URL de MLab
var baseMLabURL = 'https://api.mlab.com/api/1/databases/colbbva_oipp/collections/';
var apiKeyMLab = 'apiKey=80aJU7mCzZprpI8uqblWDwEl86jCEaNs';
//Puerto por el que esta escuchando la aplicacion
var port = process.env.PORT || 4000;
app.listen(port);

//
app.post(URLbase + 'login4', function(req, res) {
  clienteMlab = requestJSON.createClient(baseMLabURL + "/user");
  var cambio = '{"$set":' + JSON.stringify({"logged": true}) + '}';
  clienteMlab.put('?q={"email":"' + req.body.email + '","password":"' + req.body.password + '"}&' + apiKeyMLab, JSON.parse(cambio),
    function(err, resM, body) {
      //res.send(body);
      console.log('consolacfga' + body.n)
      if (body.n == 0) {
        res.send({
          'msg': 'usuario o contraseña invalidos'
        });
      } else {
        res.send({
          'msg': 'usuario logeado exitosamente!!'
        });
      }
    })
});


//POST Login con mLab
app.post(URLbase + 'login', function(req, res) {
  const httpClient = requestJSON.createClient(baseMLabURL);
  var queryString = 'q={"email":' + '"' + req.body.email + '"' + '}&';
  httpClient.get('user?' + queryString + apiKeyMLab, function(e, reqMlab, body){
    if(!body.length || !Array.isArray(body)){
      res.json({"msg":"Usuario con email errado"});
    }else{
      if(body[0].password !== req.body.password){
        res.json({"msg":"Usuario con clave errada"});
      }else{
        var login = '{"$set":' + JSON.stringify({"logged":true}) + '}';
        httpClient.put('user?q={"id":' + '"' + body[0].id + '"' + '}&' + apiKeyMLab, JSON.parse(login), function(e, reqMlab, body) {
              res.send({"login":"ok"});
              //res.json({"msg":"Usuario OK"});
          });
      }
    }
  });
  httpClient.get('user?' + queryString + apiKeyMLab, function(e, reqMlab, body){
      console.log(body);
  });
  res.status(200);
});

//POST Logout con mLab
app.post(URLbase + 'logout', function(req, res) {
    var id = req.headers.id;
    var query = 'q={"id":' + id + ', "logged":true}';
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        if (body.length == 1){
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios");
          var cambio = '{"$set":{"logged":false}}';
          clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
              res.send({"logout":"ok", "id":body[0].id});
          });
        }
        else {
          res.status(200).send('Usuario no logado previamente');
        }
      }
    });
});
