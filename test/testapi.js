//import de las librerias de test
var mocha = require('mocha');
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server');
//Instruccion que me define si es lo que espero al final de la prueba
var should = chai.should();
//Configurar chai con modulo HTTP
chai.use(chaiHttp);
//Conjunto de pruebas
describe('pruebas colombia', () => {
///*
  it('BBVA funciona', (done) => {
    chai.request('http://www.bbva.com')
    .get('/')
    .end((err, res) => {
      console.log(res);
      res.should.have.status(200);
      done();
    })
  });
//*/
///*
  it('Mi API funciona', (done) => {
    chai.request('http://localhost:4000')
    .get('/colbbva/v3/login4')
    .end((err, res) => {
      res.should.have.status(200);
      done();
    })
  });
//*/
///*
  it('Devuelve array de usuarios', (done) => {
    chai.request('http://localhost:4000')
    .get('/colbbva/v3/users')
    .end((err, res) => {
      //console.log(res.body);
      res.body.should.be.a('array');
      done();
    })
  });
//*/
///*
  it('Devuelve al menos un elemento', (done) => {
    chai.request('http://localhost:4000')
    .get('/colbbva/v3/users')
    .end((err, res) => {
      //console.log(res.body);
      res.body.length.should.be.gte(1);
      done();
    })
  });
//*/
///*
  it('Validar primer elemento', (done) => {
    chai.request('http://localhost:4000')
    .get('/colbbva/v3/users')
    .end((err, res) => {
      //console.log(res.body[0]);
      res.body[0].should.have.property('first_name');
      res.body[0].should.have.property('last_name');
      done();
    })
  });
//*/
///*
  it('Validar POST crear elemento', (done) => {
    chai.request('http://localhost:4000')
    .post('/colbbva/v3/users')
    .send('{"first_name":"Doraemon", "last_name":"Garcia"}')
    .end((err, res) => {
      console.log(body);
      done();
    })
  });
//*/

});
